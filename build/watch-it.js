var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;
var execFile = require('child_process').execFile;

var building = false;
var adl = null;

startWatching();

function startWatching() {
    console.log('Watching for changes...do work son');
    fs.watch(__dirname + '/../', handleFileWatch);

    runBuild(startApp);
}

function shouldIgnore(filename, event) {
    return filename.indexOf('.swp') >= 0;
}

function isAppRunning() {
    return adl != null;
}

function closeAdl(callback) {
    adl.on('close', callback);

    var taskKill = exec('Taskkill /IM adl.exe', function(error, stdout, stderr) {
    });
}

function didBuildFail(stdout, stderr) {
    return stdout.toLowerCase().indexOf('error') >= 0
        || stderr.toLowerCase().indexOf('error') >= 0;
}

function startApp() {
    if (isAppRunning()) {
        console.log('Restarting app');
        closeAdl(startApp);
        return;
    }

    run();
}

function runBuild(successCallback) {
    if (building === true) {
        return;
    }

    console.log('Building...');
    building = true;

    var build = execFile('build.bat', function(error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        building = false;

        if (didBuildFail(stdout, stderr)) {
            console.log('!!!!! Build Failed !!!!!');
            return;
        }

        console.log('Build successful');
        successCallback();

    });
}

function handleFileWatch(event, filename) {
    if (shouldIgnore(filename, event)) {
        return;
    }

    runBuild(startApp);
}

function run() {
    console.log('runing...');
    var args = [];
    var options = {
    };
    adl = execFile('run.bat', args, options, function(error, stdout, stderr) {
        console.log('adl closed');
        adl = null;
    });

    console.log('-----adl output-----');
    adl.stdout.on('data', function(data) {
        console.log(data);
    });
    adl.stderr.on('data', function(data) {
        console.log(data);
    });
}
