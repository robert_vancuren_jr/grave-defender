package
{
    import com.pixeltoyfactory.Game;
    import com.pixeltoyfactory.SplashScreen;
    import com.pixeltoyfactory.resources.BundleLoader;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.ProgressEvent;
    import flash.geom.Rectangle;

    import net.hires.debug.Stats;

    import starling.core.Starling;
    import starling.utils.RectangleUtil;
    import starling.utils.ScaleMode;


    [SWF(frameRate="60", width="1024", height="768", backgroundColor="0xffffff")]
    public class Main extends Sprite
    {
        private var stats:Stats;
        private var splashScreen:SplashScreen;
        private var bundleLoader:BundleLoader;

        private var starling:Starling;

        public function Main()
        {
            trace('Main');
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        private function addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            trace('added to stage');

            initialize();
        }

        private function initialize():void
        {
            addSplashScreen();
            addStats();

            loadResources();
        }

        private function addSplashScreen():void
        {
            splashScreen = new SplashScreen();
            addChild(splashScreen);
        }

        private function addStats():void
        {
            stats = new Stats();
            addChild(stats);
        }

        private function loadResources():void
        {
            bundleLoader = new BundleLoader('assets.json');
            bundleLoader.addEventListener(Event.COMPLETE, bundleCompleteHandler);
            bundleLoader.addEventListener(ProgressEvent.PROGRESS, bundleProgressHandler);
            bundleLoader.load();
        }

        private function bundleProgressHandler(event:ProgressEvent):void
        {
            splashScreen.update(event.bytesLoaded, event.bytesTotal);
        }

        private function bundleCompleteHandler(event:Event):void
        {
            trace('bundle loaded');
            removeSplashScreen();
            addGame();
        }

        private function removeSplashScreen():void
        {
            removeChild(splashScreen);
        }

        private function addGame():void
        {
            var viewPort:Rectangle = RectangleUtil.fit(
                new Rectangle(0, 0, 1024, 768),
                new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight),
                ScaleMode.SHOW_ALL
            );

            starling = new Starling(Game, stage, viewPort);
            starling.stage.stageWidth  = 1024;
            starling.stage.stageHeight = 768;

            starling.antiAliasing = 0;
            starling.showStats = true;
            starling.start();

            trace('scale factor', starling.contentScaleFactor);
        }
    }
}
