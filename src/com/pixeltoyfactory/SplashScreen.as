package com.pixeltoyfactory
{
    import flash.display.Bitmap;
    import flash.display.Graphics;
    import flash.display.Sprite;
	import flash.events.Event;

    import flash.geom.Rectangle;

	public class SplashScreen extends Sprite
	{

        [Embed(source='../../../media/images/ptf-logo.png')]
        private var Background:Class;

        private var progressBar:Sprite = new Sprite();
        private var graphics:Graphics;

		public function SplashScreen()
		{
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}

		protected function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            var bg:Bitmap = new Background();

			var unscaledBounds:Rectangle = new Rectangle(0, 0, bg.width, bg.height);
			var worldRect:Rectangle = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);

            var ratioWidth:Number = worldRect.width / unscaledBounds.width;
            var ratioHeight:Number = (worldRect.height - 200) / unscaledBounds.height;

			var appScale:Number = ratioWidth;
            if (ratioHeight < ratioWidth) {
                appScale = ratioHeight;
            }
            bg.smoothing = true;
            bg.scaleX = bg.scaleY = appScale;
            bg.x = (worldRect.width - bg.width) / 2;
            bg.y = (worldRect.height - bg.height - 100) / 2;
            addChild(bg);

            progressBar = new Sprite();
            progressBar.x = stage.fullScreenWidth / 2;
            progressBar.y = bg.y + bg.height + 20;
            progressBar.scaleX = progressBar.scaleY = appScale;
            addChild(progressBar);
            graphics = progressBar.graphics;

            update(0, 100);
		}

        public function update(current:int, total:int):void
        {
            var percent:Number = current / total;
            var width:Number = 500 * percent;

            graphics.beginFill(0x000000);
            graphics.drawRect(-width/2, 0, width, 10);
            graphics.endFill();
        }

	}

}
