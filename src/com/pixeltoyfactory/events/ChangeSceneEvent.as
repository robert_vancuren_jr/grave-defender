package com.pixeltoyfactory.events
{
    import starling.events.Event;


    public class ChangeSceneEvent extends Event
    {

        public static const CHANGE_SCENE:String = "ChangeScene";

        public var sceneName:String;
        public function ChangeSceneEvent(type:String, sceneName:String)
        {
            super(type, true, sceneName);
            this.sceneName = sceneName;
        }
    }
}
