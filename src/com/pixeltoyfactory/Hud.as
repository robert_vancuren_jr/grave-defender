package com.pixeltoyfactory
{
    import com.pixeltoyfactory.resources.ResourceBundle;
    import com.pixeltoyfactory.scenes.GamePlay;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.geom.Rectangle;

    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Sprite;
    import starling.display.Image;
    import starling.events.Event;
    import starling.text.TextField;
    import starling.textures.Texture;

    public class Hud extends Sprite
    {
        private var inventory:Inventory;
        private var score:TextField;
        private var highScore:TextField;

        private var shovelIcon:Button;
        private var shovelCoolDown:Image;

        private var arrowIcon:Button;
        private var arrowCoolDown:Image;

        private var selectionIndicator:Image;

        function Hud()
        {
            inventory = new Inventory();
            inventory.y = 768 - inventory.height;
            addChild(inventory);

            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

            var shovelIconTexture:Texture = ResourceBundle.getTexture("ShovelIcon");
            var arrowIconTexture:Texture = ResourceBundle.getTexture("ArrowIcon");

            shovelIcon = new Button(shovelIconTexture);
            shovelIcon.x = 300;
            shovelIcon.y = 768 - inventory.height + 64;
            shovelIcon.touchable = true;
            shovelIcon.addEventListener(Event.TRIGGERED, shovelTouchHandler);
            addChild(shovelIcon);

            arrowIcon = new Button(arrowIconTexture);
            arrowIcon.x = 600;
            arrowIcon.y = 768 - inventory.height + 64;
            arrowIcon.touchable = true;
            arrowIcon.addEventListener(Event.TRIGGERED, arrowTouchHandler);
            addChild(arrowIcon);


            //adfasdfasf
            var bitmapData:BitmapData = new BitmapData(64, 64, true, 0x00000000);

            var rect:Rectangle = new Rectangle(
                    0, 0, 64, 64);
            bitmapData.fillRect(rect, 0x99ff0000);

            var bitmap:Bitmap = new Bitmap(bitmapData);

            shovelCoolDown = new Image(Texture.fromBitmap(bitmap));
            shovelCoolDown.x = 300;
            shovelCoolDown.y = 768 - inventory.height + 64;
            shovelCoolDown.scaleY = 0;
            addChild(shovelCoolDown);

            arrowCoolDown = new Image(Texture.fromBitmap(bitmap));
            arrowCoolDown.x = 600;
            arrowCoolDown.y = 768 - inventory.height + 64;
            arrowCoolDown.scaleY = 0;
            addChild(arrowCoolDown);

            //adfasdfasf
            bitmapData = new BitmapData(64, 64, true, 0x00000000);

            bitmapData.fillRect(rect, 0xffff0000);

            bitmap = new Bitmap(bitmapData);

            selectionIndicator = new Image(Texture.fromBitmap(bitmap));
            selectionIndicator.x = 300 - 6;
            selectionIndicator.y = 768 - inventory.height + 64 - 6;
            selectionIndicator.scaleX = 1.2;
            selectionIndicator.scaleY = 1.2;
            addChildAt(selectionIndicator, 1);


            highScore = new TextField(100, 50, "0", "sans-serif", 30, 0xff0000);
            highScore.border = true;
            highScore.bold = true;
            highScore.hAlign = "right";
            highScore.x = 1024 - highScore.width;
            highScore.y = 0;
            addChild(highScore);

            score = new TextField(100, 50, "0", "sans-serif", 30, 0x000000);
            score.border = true;
            score.bold = true;
            score.hAlign = "right";
            score.x = 1024 - score.width;
            score.y = highScore.height;
            addChild(score);
        }

        private function addedToStageHandler(event:Event):void
        {
            stage.removeEventListener("WeaponUsed", weaponUsedHandler);
            stage.addEventListener("WeaponUsed", weaponUsedHandler);

            stage.removeEventListener("ScoreChanged", scoreChangedHandler);
            stage.addEventListener("ScoreChanged", scoreChangedHandler);
            if (GamePlay.gameData.data.highScore) {
                highScore.text = GamePlay.gameData.data.highScore.toString();
            }
            score.text = "0";
        }

        private function scoreChangedHandler(event:Event):void
        {
            trace('score changed');
            trace(event.data);
            score.text = Number(event.data).toString();
        }

        private function weaponUsedHandler(event:Event):void
        {
            var weaponType:String = String(event.data.weaponType);
            var duration:Number = Number(event.data.attackDelay);

            if (weaponType == "Shovel") {
                shovelCoolDown.scaleY = 1;
                Starling.juggler.tween(
                    shovelCoolDown, duration,
                    {
                        scaleY: 0
                    }
                );
            } else if (weaponType == "Crossbow") {
                arrowCoolDown.scaleY = 1;
                Starling.juggler.tween(
                    arrowCoolDown, duration,
                    {
                        scaleY: 0
                    }
                );

            }
        }

        private function shovelTouchHandler(event:Event):void
        {
            stage.dispatchEventWith("WeaponChange", false, "Shovel");
            selectionIndicator.x = 300 - 6;
        }

        private function arrowTouchHandler(event:Event):void
        {
            stage.dispatchEventWith("WeaponChange", false, "Arrow");
            selectionIndicator.x = 600 - 6;
        }
    }
}
