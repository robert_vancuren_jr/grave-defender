package com.pixeltoyfactory
{
    import com.pixeltoyfactory.entities.Grave;
    import com.pixeltoyfactory.entities.Zombie;
    import com.pixeltoyfactory.pathfinding.AStar;
    import com.pixeltoyfactory.pathfinding.Grid;
    import com.pixeltoyfactory.pathfinding.GridNode;
    import com.pixeltoyfactory.scenes.GamePlay;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.BitmapDataChannel;
    import flash.geom.Matrix;
    import flash.geom.Point;

    import starling.core.Starling;
    import starling.animation.DelayedCall;
    import starling.display.BlendMode;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.textures.Texture;

    public class Level
    {
        private var game:GamePlay;
        private var tileSize:int;

        private var grid:Grid;

        private var grass:Sprite;
        private var path:Sprite;

        private var spawnTime:Number = 10;
        private var spawnDelay:DelayedCall;

        private var graveContainer:Sprite;
        private var openGraves:Vector.<Grave>;

        private var gravesWithZombies:Vector.<Grave>;

        private var zombie:Zombie;

        public function Level(game:GamePlay, tileSize:int)
        {
            this.game = game;
            this.tileSize = tileSize;
        }

        public function generate():void
        {
            trace('generating level');
            removeAllZombies();
            addBackground();
        }

        public function deactivate():void
        {
            Starling.juggler.remove(spawnDelay);
        }

        private function addBackground():void
        {
            createGrid();

            addGrass();

            addGraves();

            spawnZombie();

            if (path == null) {
                path = new Sprite();
                game.playArea.addChild(path);
            }

        }

        private function spawnZombie():void {
            addZombie();
            spawnDelay = Starling.juggler.delayCall(spawnZombie, spawnTime);
            spawnTime -= 0.25;
            spawnTime = Math.max(spawnTime, 5);
        }

        private function createGrid():void
        {
            if (grid != null) {
                return;
            }

            var columns:int = 1024 / tileSize;
            var rows:int = (768 - (64 * 3)) / tileSize;

            grid = new Grid(columns, rows);
        }

        public function findPath(from:Point, to:Point):Array
        {

            grid.setStartNode(from.x, from.y);
            grid.setEndNode(to.x, to.y);

            var astar:AStar = new AStar();
            if (astar.findPath(grid)) {

                path.removeChildren();

                var pathTexture:Texture = createPathTexture();
                /*
                for each (var node:GridNode in astar.path) {
                    var tile:Image = new Image(pathTexture);
                    tile.x = node.x * tileSize;
                    tile.y = node.y * tileSize;
                    path.addChild(tile);
                }
                */

                var node:GridNode = astar.path[astar.path.length-1];
                var tile:Image = new Image(pathTexture);
                tile.x = node.x * tileSize;
                tile.y = node.y * tileSize;
                path.addChild(tile);

                return astar.path;
            } else {
                trace('no path found');
            }

            return null;
        }

        private function createPathTexture():Texture
        {
            var randomNoise:BitmapData = new BitmapData(tileSize-GamePlay.borderSize, tileSize-GamePlay.borderSize, false, 0x00000);


            var bitmapData:BitmapData = new BitmapData(tileSize, tileSize, false, 0xffffff);
            var bitmap:Bitmap = new Bitmap(bitmapData);
            var texture:Texture = Texture.fromBitmap(bitmap);

            return texture;
        }

        private function addZombie():void
        {
            var numberOfGraves:int = openGraves.length;
            if (numberOfGraves == 0) {
                return;
            }

            var randomIndex:int = Math.floor(Math.random() * numberOfGraves);
            var graveToPlaceZombieOn:Grave = openGraves[randomIndex];

            var index:int = openGraves.indexOf(graveToPlaceZombieOn);
            gravesWithZombies.push(openGraves.splice(index, 1)[0]);

            var zombie:Zombie = new Zombie();
            zombie.addEventListener("ZombieDied", zombieDiedHandler);
            zombie.touchable = false;
            zombie.x = graveToPlaceZombieOn.x + (graveToPlaceZombieOn.width - zombie.width) / 2;
            zombie.y = graveToPlaceZombieOn.y + (graveToPlaceZombieOn.height - zombie.height) / 2;

            graveToPlaceZombieOn.addZombie(zombie);
            game.playArea.addChild(zombie);
        }

        private function addGrass():void
        {
            if (grass != null) {
                return;
            }

            grass = new Sprite();

            var grassTexture:Texture = createGrassTexture();

            var columns:int = 1024 / tileSize;
            var rows:int = (768 - (64 * 3)) / tileSize;

            for (var i:int = 0; i < columns; i++) {
                for (var j:int = 0; j < rows; j++) {
                    var tile:Image = new Image(grassTexture);
                    tile.blendMode = BlendMode.NONE;
                    tile.x = i * tileSize;
                    tile.y = j * tileSize;
                    grass.addChild(tile);
                }
            }

            grass.blendMode = BlendMode.NONE;
            grass.flatten();
            game.playArea.addChild(grass);
        }

        private function createGrassTexture():Texture
        {
            var randomNoise:BitmapData = new BitmapData(tileSize-GamePlay.borderSize, tileSize-GamePlay.borderSize, false, 0x00000);
            var seed:int = int(Math.random() * int.MAX_VALUE);

            randomNoise.noise(seed, 0x66, 0xAA, BitmapDataChannel.GREEN, false);

            var bitmapData:BitmapData = new BitmapData(tileSize, tileSize, false, 0x006600);
            var transform:Matrix = new Matrix();
            transform.translate(GamePlay.borderSize/2, GamePlay.borderSize/2);
            bitmapData.draw(randomNoise, transform);

            var bitmap:Bitmap = new Bitmap(bitmapData);
            var texture:Texture = Texture.fromBitmap(bitmap);

            return texture;
        }

        private function addGraves():void
        {
            if (graveContainer != null) {
                return;
            }
            graveContainer = new Sprite();
            //graveContainer.blendMode = BlendMode.NONE;

            openGraves = new Vector.<Grave>();
            gravesWithZombies = new Vector.<Grave>();

            var columns:int = 5;
            var rows:int = 2;

            for (var i:int = 0; i < columns; i++) {
                for (var j:int = 0; j < rows; j++) {
                    var grave:Sprite = new Grave();
                    var tileX:int = ((i + 1) + ( i * 2));
                    var tileY:int = ((j + 1) + (j * 3));

                    //TODO block gridNodes
                    grid.setWalkable(tileX, tileY, false);
                    grid.setWalkable(tileX, tileY+1, false);
                    grid.setWalkable(tileX, tileY+2, false);
                    grid.setWalkable(tileX+1, tileY, false);
                    grid.setWalkable(tileX+1, tileY+1, false);
                    grid.setWalkable(tileX+1, tileY+2, false);

                    grave.x = tileSize * tileX;
                    grave.y = tileSize * tileY;

                    graveContainer.addChild(grave);
                    openGraves.push(grave);
                }
            }

            graveContainer.flatten();
            game.playArea.addChild(graveContainer);


        }

        private function removeAllZombies():void
        {
            for each (var grave:Grave in gravesWithZombies) {
                if (grave.hasZombie()) {

                    var zombie:Zombie = grave.removeZombie();
                    game.playArea.removeChild(zombie);
                }
            }

            if (openGraves != null) {
                openGraves = openGraves.concat(gravesWithZombies.splice(0, gravesWithZombies.length));
            }
        }

        private function zombieDiedHandler(event:Event):void
        {
            var deadZombie:Zombie = Zombie(event.currentTarget);
            game.playArea.removeChild(deadZombie);
            //graveToPlaceZombieOn.addZombie(zombie);
            for each (var grave:Grave in gravesWithZombies) {
                if (grave.hasZombie()) {
                    if (deadZombie === grave.getZombie()) {
                        grave.removeZombie();

                        var index:int = gravesWithZombies.indexOf(grave);
                        var removedGraves:Vector.<Grave> = gravesWithZombies.splice(index, 1);

                        openGraves.push(removedGraves[0]);
                    }
                }
            }
        }
    }
}
