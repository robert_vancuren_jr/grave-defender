package com.pixeltoyfactory.controllers
{
    import flash.geom.Point;

    import com.pixeltoyfactory.entities.Grave;
    import com.pixeltoyfactory.entities.Player;
    import com.pixeltoyfactory.pathfinding.GridNode;
    import com.pixeltoyfactory.scenes.GamePlay;

    import starling.core.Starling;
    import starling.display.DisplayObject;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;

    public class HumanPlayerController
    {
        private var player:Player;
        private var game:GamePlay;

        private var currentPath:Array;
        private var pathIndex:int = 0;
        private var destination:Point = new Point();

        private var tweenInProgress:Boolean = false;

        public function HumanPlayerController(player:Player, game:GamePlay)
        {
            this.player = player;
            this.game = game;

            addEventListeners();
        }

        private function addEventListeners():void
        {
            player.stage.addEventListener(TouchEvent.TOUCH, stageTouchHandler);
        }

        private function stageTouchHandler(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(game.playArea, TouchPhase.BEGAN);
            if (touch) {
                var localPos:Point = touch.getLocation(player.stage);

                var touched:DisplayObject = game.hitTest(localPos, true);
                if (touched == null) {
                    return;
                }

                if (touched.parent is Grave) {
                    var grave:Grave = Grave(touched.parent);

                    if (grave.hasZombie()) {
                        if (!player.attackThat(grave.getZombie())) {
                            localPos = getClosestTileToGrave(grave);
                        }
                    }
                }

                var tilePos:Point = GamePlay.pointToTile(localPos);

                if (destination.equals(tilePos)) {
                    return;
                }
                var from:Point = GamePlay.pointToTile(new Point(player.x, player.y));

                currentPath = game.level.findPath(from, tilePos);

                if (currentPath != null) {
                    pathIndex = 0;
                    if (tweenInProgress === false)
                    {
                        moveToNextWaypoint();
                    }
                }
                destination = tilePos.clone();
            }
        }

        private function getClosestTileToGrave(grave:Grave):Point
        {
            var rightSide:Point = new Point(grave.x, grave.y);
            //offset for have grave height
            rightSide.y += grave.height / 2;

            //plus one to get the tile to the right of the grave;
            rightSide.x += grave.width + 1;

            var leftSide:Point = new Point(grave.x, grave.y);
            //offset for have grave height
            leftSide.y += grave.height / 2;

            //minus one to get the tile to the left of the grave;
            leftSide.x += -1;

            var playerPos:Point = new Point(player.x, player.y);
            return closestTo(playerPos, rightSide, leftSide);
        }

        private function moveToNextWaypoint():void
        {
            var nextNode:GridNode = currentPath[pathIndex];
            var tilePos:Point = new Point(nextNode.x, nextNode.y);

            var newPlayerPos:Point = GamePlay.pointFromTile(tilePos);

            var currentPos:Point = new Point(player.x, player.y);
            var distance:Number = Point.distance(currentPos, newPlayerPos);

            var delay:Number = (distance / 100) * 0.5;


            tweenInProgress = true;
            Starling.juggler.tween(
                player, delay,
                {
                    onComplete: tweenComplete,
                    x: newPlayerPos.x,
                    y: newPlayerPos.y
                }
            );

            pathIndex++;
        }

        private function tweenComplete():void
        {
            tweenInProgress = false;
            if (currentPath == null) {
                return;
            }

            if (pathIndex < currentPath.length) {

                if (pathIndex === 0) {
                    var nextNode:GridNode = currentPath[pathIndex + 1];
                    var tilePos:Point = new Point(nextNode.x, nextNode.y);
                    var currentPos:Point = new Point(player.x, player.y);
                    var currentTile:Point = GamePlay.pointToTile(currentPos);

                    if (tilePos.equals(currentTile)) {
                        pathIndex++;
                    }
                }

                moveToNextWaypoint();
            }
        }

        private function movePlayerTo(point:Point):void
        {
            var tilePos:Point = GamePlay.pointToTile(point);

            var newPlayerPos:Point = GamePlay.pointFromTile(tilePos);

            var currentPos:Point = new Point(player.x, player.y);
            var distance:Number = Point.distance(currentPos, newPlayerPos);

            var delay:Number = distance / 100;

        }

        private function closestTo(from:Point, point1:Point, point2:Point):Point
        {
            var dist1:Number = Point.distance(from, point1);
            var dist2:Number = Point.distance(from, point2);

            if (dist1 <= dist2) {
                return point1;
            }

            return point2;
        }
    }
}
