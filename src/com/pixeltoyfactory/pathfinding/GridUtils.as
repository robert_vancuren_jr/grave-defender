package com.pixeltoyfactory.pathfinding
{
	import as3isolib.core.IsoDisplayObject;
	import as3isolib.display.primitive.IsoBox;
	import as3isolib.display.scene.IsoScene;
	
	import flash.geom.Rectangle;
	
	public class GridUtils
	{
		public function GridUtils()
		{
		}

		public static function fillGridFromIsoScene(pathGrid:Grid, isoScene:IsoScene):void
		{	
			for each (var node:IsoDisplayObject in isoScene.children)
			{
				if (node.data != null && node.data.blocking != null && node.data.blocking == true)
				{
					var xpos:int = Math.floor(node.x / pathGrid.tileSize);
					var ypos:int = Math.floor(node.y / pathGrid.tileSize);
					
					var missCount:int = 0;
					var done:Boolean = false;
					while (!done)
					{
						var gridRect:Rectangle = new Rectangle(xpos * pathGrid.tileSize,
																ypos * pathGrid.tileSize,
																pathGrid.tileSize,
																pathGrid.tileSize);
																
						var nodeRect:Rectangle = new Rectangle(node.x, node.y, node.width, node.length);
						
						if (xpos < pathGrid.numCols && nodeRect.intersects(gridRect))
						{
							pathGrid.setWalkable(xpos, ypos, false);
							xpos++;
							missCount = 0;
						}
						else
						{
							xpos = Math.floor(node.x / pathGrid.tileSize);
							ypos++;
							missCount++
							if (missCount > 1 || ypos >= pathGrid.numRows)
							{
								done = true;
							}
						}
					}
				}
			}
		}
		
		public static function renderGridToIsoScene(pathGrid:Grid, isoScene:IsoScene):void
		{
			isoScene.removeAllChildren();
			for(var i:int = 0; i < pathGrid.numCols; i++)
			{
				for(var j:int = 0; j < pathGrid.numRows; j++)
				{
					var node:GridNode = pathGrid.getNode(i, j);
					
					if (node.walkable == false)
					{
						var box:IsoBox = new IsoBox();
						box.setSize(pathGrid.tileSize, pathGrid.tileSize, 2);
						box.moveTo(i * pathGrid.tileSize, j * pathGrid.tileSize, 0)
						isoScene.addChild(box);
					}
					
				}
			}
			
			isoScene.render();
		}
	}
}