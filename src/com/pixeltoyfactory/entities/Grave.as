package com.pixeltoyfactory.entities
{
    import com.pixeltoyfactory.resources.ResourceBundle;
    import com.pixeltoyfactory.scenes.GamePlay;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.BitmapDataChannel;
    import flash.geom.Matrix;

    import starling.display.BlendMode;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.Texture;

    public class Grave extends Sprite
    {
        private static var dirtTexture:Texture;

        private var headStoneTexture:Texture;

        private var zombie:Zombie;

        public function Grave()
        {
            trace('New Grave');
            super();


            headStoneTexture = ResourceBundle.getTexture("HeadStone");

            init();

            flatten();
        }

        public function addZombie(zombie:Zombie):void
        {
            this.zombie = zombie;
        }

        public function removeZombie():Zombie
        {
            var temp:Zombie = zombie;
            zombie = null;
            return temp;
        }

        public function hasZombie():Boolean
        {
            return zombie !== null;
        }

        public function getZombie():Zombie
        {
            return zombie;
        }

        private function init():void
        {
            if (Grave.dirtTexture == null) {
                Grave.dirtTexture = createDirtTexture();
            }

            createGrave();
        }

        private function createGrave():void
        {
            for (var i:int = 0; i < 2; i++) {
                for (var j:int = 0; j < 3; j++) {
                    var tile:Image = new Image(Grave.dirtTexture);
                    tile.x = i * GamePlay.tileSize;
                    tile.y = j * GamePlay.tileSize;
                    addChild(tile);
                }
            }

            var headStone:Image = new Image(headStoneTexture);
            headStone.y = -headStone.height / 2;
            addChild(headStone);
        }


        private function createDirtTexture():Texture
        {
            var randomNoise:BitmapData = new BitmapData(GamePlay.tileSize-GamePlay.borderSize, GamePlay.tileSize-GamePlay.borderSize, false, 0x00000);
            var seed:int = int(Math.random() * int.MAX_VALUE);

            randomNoise.noise(seed, 0x66, 0xAA, BitmapDataChannel.RED | BitmapDataChannel.GREEN, false);

            var bitmapData:BitmapData = new BitmapData(GamePlay.tileSize, GamePlay.tileSize, false, 0x964B00);
            var transform:Matrix = new Matrix();
            transform.translate(GamePlay.borderSize/2, GamePlay.borderSize/2);
            bitmapData.draw(randomNoise, transform);

            var bitmap:Bitmap = new Bitmap(bitmapData);
            var texture:Texture = Texture.fromBitmap(bitmap);

            return texture;
        }
    }
}
