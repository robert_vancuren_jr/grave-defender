package com.pixeltoyfactory.entities
{
    import com.pixeltoyfactory.resources.ResourceBundle;
    import com.pixeltoyfactory.weapons.Crossbow;
    import com.pixeltoyfactory.weapons.Shovel;
    import com.pixeltoyfactory.weapons.Weapon;

    import flash.display.Bitmap;
    import flash.display.BitmapData;

    import starling.animation.DelayedCall;
    import starling.core.Starling;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.textures.Texture;

    public class Player extends Sprite
    {
        public var attackTarget:Zombie;

        public var container:Sprite;
        private var player:Image;
        private var shovel:Image;
        private var delayedAttack:DelayedCall;

        private var shovelWeapon:Shovel;
        private var crossbow:Crossbow;

        private var selectedWeapon:Weapon;

        public function Player()
        {
            trace('New Player');
            init();
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        public function addedToStageHandler(event:Event):void {
            stage.addEventListener("WeaponChange", weaponChangeHandler);
        }

        private function weaponChangeHandler(event:Event):void
        {
            var changeTo:String = String(event.data);
            if (changeTo == "Shovel") {
                selectedWeapon = shovelWeapon;
            } else if (changeTo == "Arrow") {
                selectedWeapon = crossbow;
            }
        }

        public function attackThat(zombie:Zombie):Boolean
        {
            attackTarget = zombie;
            faceTarget();
            if (selectedWeapon.isInRange(zombie)) {
                if (selectedWeapon.attack(zombie)) {
                    animateAttack();
                }
                return true;
            }

            return false
        }

        private function faceTarget():void {
            if (x < attackTarget.x) {
                container.scaleX = -1;
            } else {
                container.scaleX = 1;
            }
        }

        private function animateAttack():void
        {

            Starling.juggler.tween(
                shovel, 0.2,
                {
                    rotation: -Math.PI / 2,
                    onComplete: tweenComplete
                }
            );
        }

        private function tweenComplete():void
        {
            shovel.rotation = 0;
        }

        private function init():void
        {

            container = new Sprite();
            container.pivotX = 128 / 2;
            container.x = 64 / 2;

            addChild(container);
            var playerTexture:Texture = ResourceBundle.getTexture("Player");
            player = new Image(playerTexture);


            container.addChild(player);

            var shovelTexture:Texture = ResourceBundle.getTexture("Shovel");
            shovel = new Image(shovelTexture);
            shovel.pivotX = shovel.width / 2;
            shovel.pivotY = 90;

            shovel.x = 40;
            shovel.y = 64;
            container.y = -container.height + 55;;
            container.addChild(shovel);


            //Create weapons
            shovelWeapon = new Shovel(this);
            shovelWeapon.x = shovel.x;
            shovelWeapon.y = shovel.y;
            container.addChild(shovelWeapon);

            crossbow = new Crossbow(this);
            crossbow.x = shovel.x;
            crossbow.y = shovel.y;
            container.addChild(crossbow);


            selectedWeapon = shovelWeapon;
        }
    }
}
