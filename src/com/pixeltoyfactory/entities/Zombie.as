package com.pixeltoyfactory.entities
{
    import com.pixeltoyfactory.events.ChangeSceneEvent;
    import com.pixeltoyfactory.resources.ResourceBundle;

    import starling.animation.DelayedCall;
    import starling.core.Starling;
    import starling.events.Event;
    import starling.display.Image;
    import starling.display.MovieClip;
    import starling.display.Sprite;
    import starling.textures.Texture;

    public class Zombie extends Sprite
    {
        private var breakoutCall:DelayedCall;
        private var hand:MovieClip;
        public var health:int = 100;

        public function Zombie()
        {
            super();
            init();
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
        }

        public function takeDamage(amount:int):void
        {
            health -= amount;

            if (health <= 0) {
                Starling.juggler.remove(breakoutCall);


                dispatchEvent(new Event("ZombieDied", true));
            }
        }

        private function init():void
        {
            var handOpened:Texture = ResourceBundle.getTexture("ZombieHandOpened");
            var handClosed:Texture = ResourceBundle.getTexture("ZombieHandClosed");
            var textures:Vector.<Texture> = new Vector.<Texture>(2, true);
            textures[0] = handOpened;
            textures[1] = handClosed;

            hand = new MovieClip(textures, 1);
            addChild(hand);
        }

        private function addedToStageHandler(event:Event):void
        {
            Starling.juggler.add(hand);

            breakoutCall = Starling.juggler.delayCall(showZombie, 30);
        }

        private function showZombie():void
        {
            removeChild(hand);
            var zombieTexture:Texture = ResourceBundle.getTexture("Zombie");
            var zombie:Image = new Image(zombieTexture);
            zombie.x -= hand.width / 2;
            zombie.y -= hand.height / 2;

            addChild(zombie);
            Starling.juggler.delayCall(endGame, 2);
        }

        private function removedFromStageHandler(event:Event):void
        {
            Starling.juggler.remove(hand);

            if (breakoutCall) {
                Starling.juggler.remove(breakoutCall);
            }
        }

        private function endGame():void
        {
            stage.dispatchEventWith("GameOver");
        }
    }
}
