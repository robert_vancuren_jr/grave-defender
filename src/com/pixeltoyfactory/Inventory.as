package com.pixeltoyfactory
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.geom.Rectangle;

    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.Texture;

    public class Inventory extends Sprite
    {
        function Inventory()
        {
            trace('Inventory');
            addBackground();
        }

        private function addBackground():void
        {
            var backgroundTexture:Texture = createBackgroundTexture();
            var background:Image = new Image(backgroundTexture);
            addChild(background);
            trace('all done');
        }

        private function createBackgroundTexture():Texture
        {
            var bitmapData:BitmapData = new BitmapData(1024, 64 * 3, false, 0x000000);

            var rect:Rectangle = new Rectangle(
                    0, 0, 1024, 64 * 3);
            bitmapData.fillRect(rect, 0xff0000);

            rect = new Rectangle(
                    2, 2, 1020, (64 * 3)-4);
            bitmapData.fillRect(rect, 0x9999ff);


            var bitmap:Bitmap = new Bitmap(bitmapData);

            return Texture.fromBitmap(bitmap);
        }
    }
}
