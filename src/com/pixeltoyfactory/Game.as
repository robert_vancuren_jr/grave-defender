package com.pixeltoyfactory
{
    import com.pixeltoyfactory.events.ChangeSceneEvent;
    import com.pixeltoyfactory.scenes.MainMenu;
    import com.pixeltoyfactory.scenes.GamePlay;

    import flash.events.DataEvent;

    import starling.display.Sprite;
    import starling.events.Event;

    public class Game extends Sprite
    {
        private var currentScene:Sprite;
        private var mainMenu:MainMenu;
        private var gamePlay:GamePlay;

        public function Game()
        {
            super();
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        private function addedToStageHandler(event:Event):void
        {
            trace("Game added to stage");
            gamePlay = new GamePlay();

            mainMenu = new MainMenu();
            addEventListener(ChangeSceneEvent.CHANGE_SCENE, changeSceneHandler);

            dispatchEvent(new ChangeSceneEvent(ChangeSceneEvent.CHANGE_SCENE, "MainMenu"));
        }

        private function changeSceneHandler(event:ChangeSceneEvent):void
        {
            trace("!got a ChangeScene event", event.sceneName);
            if (currentScene) {
                removeChild(currentScene);
            }

            currentScene = getSceneByName(event.sceneName);
            trace('new scene', currentScene);
            addChild(currentScene);
        }

        private function getSceneByName(sceneName:String):Sprite
        {
            var scene:Sprite;

            switch(sceneName) {
                case "MainMenu":
                    scene = mainMenu;
                    break;
                case "GamePlay":
                    scene = gamePlay;
                    break;

            }
            return scene;
        }
    }
}
