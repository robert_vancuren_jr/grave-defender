package com.pixeltoyfactory.weapons
{
    import com.pixeltoyfactory.entities.Player;
    import com.pixeltoyfactory.resources.ResourceBundle;
    import com.pixeltoyfactory.scenes.GamePlay;

    import flash.geom.Point;

    import starling.core.Starling;
    import starling.display.DisplayObjectContainer;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.textures.Texture;

    public class Crossbow extends Weapon
    {
        private var arrow:Image;
        private var arrowTexture:Texture;

        function Crossbow(player:Player)
        {
            super(player);
            trace('New Crossbow');

            weaponType = "Crossbow";
            range = 2000;
            attackDelay = 4;
            damage = 100 / 2;

            var stage:DisplayObjectContainer = GamePlay.getStage();

            arrowTexture = ResourceBundle.getTexture("Arrow");
        }

        private function createArrow():Image
        {
            var arrow:Image = new Image(arrowTexture);

            arrow.x = 500;
            arrow.y = 500;

            stage.addChild(arrow);
            return arrow;
        }

        override protected function playAnimation():void {
            trace('draw that arrow');
            var currentPos:Point = new Point(x, y);
            currentPos = parent.localToGlobal(currentPos);

            var targetPos:Point = new Point(attacking.x, attacking.y);
            targetPos = attacking.parent.localToGlobal(targetPos);

            var distance:Number = Point.distance(currentPos, targetPos);

            arrow = createArrow();
            arrow.x = currentPos.x;
            arrow.y = currentPos.y;

            Starling.juggler.tween(
                arrow, 0.002 * distance,
                {
                    onComplete: tweenComplete,
                    x: targetPos.x,
                    y: targetPos.y
                }
            );


        }

        private function tweenComplete():void
        {
            arrow.parent.removeChild(arrow);
            attacking.takeDamage(damage);
        }

    }
}
