package com.pixeltoyfactory.weapons
{
    import com.pixeltoyfactory.entities.Player;
    import com.pixeltoyfactory.entities.Zombie;

    import flash.geom.Point;

    import starling.core.Starling;
    import starling.display.Sprite;

    public class Weapon extends Sprite
    {
        protected var range:Number = 64 * 2;
        protected var attackDelay:Number = 1; //Seconds

        protected var readyToAttack:Boolean = true;
        protected var damage:Number = 50;

        protected var attacking:Zombie;

        protected var player:Player;
        protected var weaponType:String = "Weapon";

        function Weapon(player:Player)
        {
            super();
            trace('New Weapon');
            this.player = player;
        }

        public function attack(target:Zombie):Boolean
        {
            if (!readyToAttack) {
                return false;
            }

            attacking = target;

            var eventData:Object = {};
            eventData.weaponType = weaponType;
            eventData.attackDelay = attackDelay;
            stage.dispatchEventWith("WeaponUsed", false, eventData);
            playAnimation();
            readyToAttack = false;
            Starling.juggler.delayCall(delayComplete, attackDelay);

            return true;
        }

        protected function playAnimation():void
        {
            attacking.takeDamage(damage);
        }

        private function delayComplete():void
        {
            readyToAttack = true;
        }

        public function isInRange(target:Sprite):Boolean
        {
            var currentPos:Point = new Point(x, y);
            currentPos = parent.localToGlobal(currentPos);

            var targetPos:Point = new Point(target.x, target.y);
            targetPos = target.parent.localToGlobal(targetPos);
            var distance:Number = Point.distance(currentPos, targetPos);

            return distance <= range;
        }
    }
}
