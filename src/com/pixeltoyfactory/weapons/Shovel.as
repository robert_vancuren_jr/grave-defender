package com.pixeltoyfactory.weapons
{
    import com.pixeltoyfactory.entities.Player;

    public class Shovel extends Weapon
    {
        function Shovel(player:Player)
        {
            super(player);
            trace('New Shovel');

            weaponType = "Shovel";
            range = 64 * 2;
            attackDelay = 0.5;
            damage = 100 / 5;
        }
    }
}
