package com.pixeltoyfactory.resources
{
	import flash.display.Bitmap;
    import flash.display.Loader;
    import flash.display.LoaderInfo;

    import flash.filesystem.File;
    import flash.filesystem.FileStream;
    import flash.filesystem.FileMode;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.ProgressEvent;
    import flash.events.IOErrorEvent;

    import flash.system.LoaderContext;
    import flash.system.ImageDecodingPolicy;

    import flash.utils.ByteArray;

    public class ResourceLoader extends EventDispatcher
    {
        public var resource:Object;
        public var resourceName:String;


        private var resourceObject:Object;

        public function ResourceLoader()
        {
        }

        public function load(resourceObject:Object):void
        {
            this.resourceObject = resourceObject;
            var appDirectory:File = File.applicationDirectory;

            var assetFile:File = appDirectory.resolvePath(resourceObject.path);

            var assetStream:FileStream = new FileStream();
            assetStream.addEventListener(IOErrorEvent.IO_ERROR, fileIoErrorHandler);
            assetStream.addEventListener(ProgressEvent.	PROGRESS, fileProgressHandler);
            assetStream.addEventListener(Event.COMPLETE, fileCompleteHandler);
            assetStream.openAsync(assetFile, FileMode.READ);
        }

        private function fileIoErrorHandler(event:IOErrorEvent):void
        {
        }

        private function fileProgressHandler(event:ProgressEvent):void
        {
        }

        private function fileCompleteHandler(event:Event):void
        {
            var fileStream:FileStream = FileStream(event.currentTarget);

            var fileBytes:ByteArray = new ByteArray();
            fileStream.readBytes(fileBytes, 0, fileStream.bytesAvailable);

            if (resourceObject.assetType === "Bitmap") {
                var loaderContext:LoaderContext = new LoaderContext();
                loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD
                var loader:Loader = new Loader();


                loader.loadBytes(fileBytes, loaderContext);
                loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaderCompleteHandler);
            } else if (resourceObject.assetType == "XML") {
                var xmlString:String = fileBytes.readUTFBytes(fileBytes.bytesAvailable);

                //Assets.assets[resourceObject.assetName] = new XML(xmlString);
                resource = new XML(xmlString);
                resourceName = resourceObject.assetName;
                dispatchEvent(new Event(Event.COMPLETE));
            } else {
                throw new Error("Unknown Asset Type " + resourceObject.assetType);
            }
        }

        private function loaderCompleteHandler(event:Event):void
        {
            var loaderInfo:LoaderInfo = LoaderInfo(event.currentTarget);
            var loader:Loader = loaderInfo.loader;

            //Assets.assets[resourceObject.assetName] = Bitmap(loader.content);
            resource = Bitmap(loader.content);
            resourceName = resourceObject.assetName;


            dispatchEvent(new Event(Event.COMPLETE));
        }
    }
}
