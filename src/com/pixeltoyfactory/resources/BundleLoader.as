package com.pixeltoyfactory.resources
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.ProgressEvent;
    import flash.events.IOErrorEvent;

    import flash.filesystem.File;
    import flash.filesystem.FileStream;
    import flash.filesystem.FileMode;

    import flash.system.Capabilities;
    import flash.system.LoaderContext;
    import flash.system.ImageDecodingPolicy;

    import flash.utils.ByteArray;


    public class BundleLoader extends EventDispatcher
    {
        private var manifestFilePath:String;

        public function BundleLoader(manifestFilePath:String)
        {
            this.manifestFilePath = manifestFilePath;
        }

        public function load():void
        {
            trace('loading resources');
            var appDirectory:File = File.applicationDirectory;

            //maybe we should just read a directory for assets??
            //that seems like it would be much more win
            var assetFile:File = appDirectory.resolvePath(manifestFilePath);

            var assetStream:FileStream = new FileStream();
            assetStream.addEventListener(IOErrorEvent.IO_ERROR, fileIoErrorHandler);
            assetStream.addEventListener(Event.COMPLETE, fileCompleteHandler);
            assetStream.openAsync(assetFile, FileMode.READ);

        }

        private function fileIoErrorHandler(event:IOErrorEvent):void
        {
            trace("IOError", event);
        }

        private function assetProgressHandler(event:ProgressEvent):void
        {
            dispatchEvent(event);
        }

        private function fileCompleteHandler(event:Event):void
        {
            var fileStream:FileStream = FileStream(event.currentTarget);

            fileStream.removeEventListener(IOErrorEvent.IO_ERROR, fileIoErrorHandler);
            fileStream.removeEventListener(Event.COMPLETE, fileCompleteHandler);

            var json:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
            var assetMap:Object = JSON.parse(json);
            var bundle:ResourceBundle = new ResourceBundle();
            bundle.addEventListener(Event.COMPLETE, assetLoaderCompleteHandler);
            bundle.addEventListener(ProgressEvent.PROGRESS, assetProgressHandler);

            bundle.loadAssets(assetMap.assets);
        }

        private function assetLoaderCompleteHandler(event:Event):void
        {
            dispatchEvent(new Event(Event.COMPLETE));
        }
    }
}
