package com.pixeltoyfactory.resources
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.ProgressEvent;
    import flash.display.Bitmap;
    import flash.filesystem.File;
    import flash.utils.Dictionary;

    import starling.textures.Texture;
    import starling.textures.TextureAtlas;

	public class ResourceBundle extends EventDispatcher
	{
        public static var resources:Object = {};
        public static var textures:Dictionary = new Dictionary();

        private var numberOfAssets:int;
        private var numberLoaded:int = 0;

        private var assetsLoaded:Boolean = false;

        public static function getTexture(name:String):Texture
		{
			if (textures[name] == undefined)
			{
                var bitmap:Bitmap;
                if (resources[name] is Class) {
				    bitmap = new resources[name]();
                } else {
				    bitmap = resources[name];
                }

				textures[name] = Texture.fromBitmap(bitmap);
			}
			return textures[name];
		}

        public function ResourceBundle()
        {
        }

		public function loadAssets(assetsToLoad:Array):void
		{
            for each (var assetObject:Object in assetsToLoad) {
                numberOfAssets++;
                if (assetObject.assetType != 'Sound') {
                    var assetLoader:ResourceLoader = new ResourceLoader();
                    assetLoader.addEventListener(Event.COMPLETE, assetLoaderCompleteHandler);
                    assetLoader.load(assetObject);
                } else {
                    var soundLoader:SoundResourceLoader = new SoundResourceLoader();
                    soundLoader.addEventListener(Event.COMPLETE, assetLoaderCompleteHandler);
                    soundLoader.load(assetObject);
                }
            }

		}

        private function assetLoaderCompleteHandler(event:Event):void
        {
            numberLoaded++;

            var resouceLoader:Object = event.currentTarget;
            resources[resouceLoader.resourceName] = resouceLoader.resource;

            var progressEvent:ProgressEvent = new ProgressEvent(
                    ProgressEvent.PROGRESS,
                    false, false, numberLoaded, numberOfAssets);

            dispatchEvent(progressEvent);

            if (numberLoaded == numberOfAssets) {
                assetsLoaded = true;
                dispatchEvent(new Event(Event.COMPLETE));
            }
        }
	}
}
