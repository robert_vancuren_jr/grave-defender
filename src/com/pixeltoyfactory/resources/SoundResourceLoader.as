package com.pixeltoyfactory.resources
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;

    import flash.filesystem.File;

    import flash.net.URLRequest;

    import flash.media.Sound;

    public class SoundResourceLoader extends EventDispatcher
    {
        public var resource:Object;
        public var resourceName:String;


        private var resourceObject:Object;

        public function SoundResourceLoader()
        {
        }

        public function load(resourceObject:Object):void
        {
            this.resourceObject = resourceObject;
            var appDirectory:File = File.applicationDirectory;
            var assetFile:File = appDirectory.resolvePath(resourceObject.path);

            var request:URLRequest = new URLRequest(assetFile.url);

            var soundFactory:Sound = new Sound();
            soundFactory.addEventListener(Event.COMPLETE, completeHandler);
            soundFactory.addEventListener(Event.ID3, id3Handler);
            soundFactory.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            soundFactory.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            soundFactory.load(request);

        }

        private function completeHandler(event:Event):void {
            resource = event.currentTarget;
            resourceName = resourceObject.assetName;
            dispatchEvent(event);
        }

        private function id3Handler(event:Event):void {
        }

        private function ioErrorHandler(event:Event):void {
        }

        private function progressHandler(event:ProgressEvent):void {
        }

    }
}
