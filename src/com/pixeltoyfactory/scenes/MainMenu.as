package com.pixeltoyfactory.scenes
{
    import com.pixeltoyfactory.events.ChangeSceneEvent;
    import com.pixeltoyfactory.resources.ResourceBundle;

    import starling.display.BlendMode;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.textures.Texture;

    public class MainMenu extends Sprite
    {
        private var background:Image;
        private var playButton:Button;

        public function MainMenu()
        {
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        private function addedToStageHandler(event:Event):void
        {
            trace("MainMenu added to stage");
            addBackground();
            addPlayButton();
        }

        private function addBackground():void
        {
            var backgroundTexture:Texture = ResourceBundle.getTexture("MainMenuBg");
            background = new Image(backgroundTexture);
            background.blendMode = BlendMode.NONE;

            addChild(background);
        }

        private function addPlayButton():void
        {
            var buttonTexture:Texture = ResourceBundle.getTexture("PlayButton");
            var button:Button = new Button(buttonTexture);

            button.x = (stage.stageWidth - button.width) / 2;
            button.y = (stage.stageHeight - button.height) / 2;

            button.addEventListener(Event.TRIGGERED, playButtonTriggeredHandler);

            addChild(button);
        }

        private function playButtonTriggeredHandler(event:Event):void
        {
            trace("play button clicked");
            dispatchEvent(new ChangeSceneEvent(ChangeSceneEvent.CHANGE_SCENE, "GamePlay"));
        }
    }
}
