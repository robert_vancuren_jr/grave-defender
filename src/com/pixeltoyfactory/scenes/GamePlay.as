package com.pixeltoyfactory.scenes
{
    import com.pixeltoyfactory.Hud;
    import com.pixeltoyfactory.Level;
    import com.pixeltoyfactory.controllers.HumanPlayerController;
    import com.pixeltoyfactory.events.ChangeSceneEvent;
    import com.pixeltoyfactory.entities.Grave;
    import com.pixeltoyfactory.entities.Player;
    import com.pixeltoyfactory.resources.ResourceBundle;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.BitmapDataChannel;
    import flash.events.TimerEvent;
    import flash.geom.Matrix;
    import flash.geom.Point;
    import flash.net.SharedObject;
    import flash.utils.Timer;

    import starling.display.Button;
    import starling.display.DisplayObjectContainer;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.textures.Texture;

    public class GamePlay extends Sprite
    {
        public static var tileSize:int = 64;
        public static var borderSize:int = 4;
        public static var gameStage:DisplayObjectContainer;
        public static var gameData:SharedObject;

        private var player:Player;
        private var playerController:HumanPlayerController;

        private var score:Number = 0;

        private var initialized:Boolean = false;

        private var hud:Hud;

        public var level:Level;
        public var playArea:Sprite;

        public static function getStage():DisplayObjectContainer
        {
            return gameStage;
        }

        public static function pointToTile(point:Point):Point
        {
            var tilePos:Point = new Point();
            tilePos.x = Math.floor(point.x / GamePlay.tileSize);
            tilePos.y = Math.floor(point.y / GamePlay.tileSize);

            return tilePos;
        }

        public static function pointFromTile(tilePos:Point):Point
        {
            var point:Point = new Point();
            point.x = tilePos.x * GamePlay.tileSize;
            point.y = tilePos.y * GamePlay.tileSize;

            return point;
        }

        public function GamePlay()
        {
            super();
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
            gameData = SharedObject.getLocal('GraveDefender');
            trace('------------');
            trace(gameData);
            trace(gameData.data);
            trace(gameData.data.highScore);
        }


        private function addedToStageHandler(event:Event):void
        {

            score = 0;
            gameStage = stage;
            trace("GamePlay added to stage");
            if (initialized === false) {
                playArea = new Sprite();
                hud = new Hud();
                addChild(playArea);
                init();
            }

            level.generate();

            addChildAt(player, numChildren - 1);

            addChild(hud);
            trace('added hud');
        }

        private function removedFromStageHandler(event:Event):void
        {
            trace('removed clean up');
            level.deactivate();
        }

        private function init():void
        {

            level = new Level(this, tileSize);

            addPlayer();

            initialized = true;

            stage.addEventListener("ZombieDied", enemyKilledHandler);
            stage.addEventListener("GameOver", gameOberHandler);
        }

        private function gameOberHandler(event:Event):void
        {
            trace('game over score', score);

            if (gameData.data.highScore) {
                if (score > Number(gameData.data.highScore)) {
                    gameData.data.highScore = score;
                    gameData.flush();
                    trace('new high score');
                    trace(gameData.data);
                }
            } else {
                gameData.data.highScore = score;
                gameData.flush();
                trace('saved high score for the first time');
            }

            dispatchEvent(new ChangeSceneEvent(ChangeSceneEvent.CHANGE_SCENE, "MainMenu"));
        }

        private function enemyKilledHandler(event:Event):void
        {
            score += 50;
            stage.dispatchEventWith("ScoreChanged", false, score);
        }

        private function addPlayer():void
        {
            player = new Player();
            player.x = 0;
            player.y = 0;

            addChild(player);

            playerController = new HumanPlayerController(player, this);
        }

    }
}
